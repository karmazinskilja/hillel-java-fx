module ua.ithillel.hilleljavafx {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens ua.ithillel.hilleljavafx to javafx.fxml;
    exports ua.ithillel.hilleljavafx;
    exports ua.ithillel.hilleljavafx.userapp;
    exports ua.ithillel.hilleljavafx.homework;
}