package ua.ithillel.hilleljavafx.homework.datamapping;

import ua.ithillel.hilleljavafx.homework.datamapping.entity.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileDataMapper {

    private final String filePath;

    public FileDataMapper(String filePath) {
        this.filePath = filePath;
    }

    public List<User> toUserList() {
        List<User> usersList = new ArrayList<>();
        try {
            String fileContent = new String(Files.readAllBytes(Paths.get(filePath)));
            Scanner reader = new Scanner(fileContent);
            while (reader.hasNextLine()) {
                usersList.add(lineToUser(reader.nextLine()));
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
        return usersList;
    }

    private User lineToUser(String line) {
        String[] toFieldsArray = line.split("\\|");
        return new User(toFieldsArray[0].substring(9), toFieldsArray[1].substring(9));
    }

}
