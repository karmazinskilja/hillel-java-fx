package ua.ithillel.hilleljavafx.homework.datamapping.entity;

public record User(String username, String password) {
}
