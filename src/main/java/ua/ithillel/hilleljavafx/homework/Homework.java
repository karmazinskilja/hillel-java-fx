package ua.ithillel.hilleljavafx.homework;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ua.ithillel.hilleljavafx.homework.datamapping.FileDataMapper;
import ua.ithillel.hilleljavafx.homework.datamapping.entity.User;

import java.util.Objects;

public class Homework extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Login form");


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);

        Text login = new Text("Login");
        login.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        grid.add(login, 0, 0, 2, 1);

        Text userNameLabel = new Text("User Name:");
        userNameLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        grid.add(userNameLabel, 0, 1);

        TextField userNameField = new TextField();
        userNameField.setPrefSize(150, 12);
        grid.add(userNameField, 1, 1);

        Text passwordLabel = new Text("Password:");
        passwordLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        grid.add(passwordLabel, 0, 2);

        PasswordField passwordField = new PasswordField();
        passwordField.setPrefSize(150, 12);
        grid.add(passwordField, 1, 2);

        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        btn.setStyle("-fx-background-color: #1DC119");
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);


        primaryStage.setScene(scene);
        primaryStage.show();


        btn.setOnAction(e -> {
            FileDataMapper dataMapper = new FileDataMapper("src/main/resources/users.txt");
            for (User user : dataMapper.toUserList()) {
                if (Objects.equals(userNameField.getText(), user.username()) && Objects.equals(passwordField.getText(), user.password())) {
                    primaryStage.close();

                    BorderPane root = new BorderPane();
                    Scene welcomeScene = new Scene(root, 300, 275);
                    VBox vBox = new VBox();
                    Text text = new Text("Welcome to App, " + user.username());
                    text.setFont(Font.font("Arial", FontWeight.NORMAL, 18));
                    vBox.getChildren().add(text);
                    vBox.setAlignment(Pos.CENTER);
                    root.setCenter(vBox);

                    primaryStage.setScene(welcomeScene);
                    primaryStage.show();
                }
            }
            Text actionTarget = new Text();
            grid.add(actionTarget, 1, 3);
            actionTarget.setFill(Color.FIREBRICK);
            actionTarget.setText("Incorrect login");
        });
    }


}
